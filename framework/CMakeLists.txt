# First, process the 'CMakeLists.txt' files from child-directories.
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/basecalls) # Sets `BASECALL_SOURCES`
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/configuration) # Sets `CONFIGURATION_SOURCES`
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/execution_planning) # Sets `EXECUTION_PLANNING_SOURCES`
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/fragments) # Sets `FRAGMENT_SOURCES`
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/python) # Sets `PYTHON_SOURCES`

# Then, create the variable `FRAMEWORK_SOURCES` with file paths to files that should be linked.
# Add the subdirectories to `FRAMEWORK_SOURCES`
# The parameter `PARENT_SCOPE` exposes this variable to the parent directory.
set(FRAMEWORK_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/CycleManager.h
        ${CMAKE_CURRENT_SOURCE_DIR}/CycleManager.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/FileDetector.h
        ${CMAKE_CURRENT_SOURCE_DIR}/FileDetector.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Framework.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Framework.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/FrameworkInterface.h
        ${CMAKE_CURRENT_SOURCE_DIR}/FrameworkInterface.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/main.h
        ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Plugin.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Serializable.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Serializable.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/utils.h
        ${CMAKE_CURRENT_SOURCE_DIR}/utils.cpp
        ${BASECALL_SOURCES}
        ${CONFIGURATION_SOURCES}
        ${EXECUTION_PLANNING_SOURCES}
        ${FRAGMENT_SOURCES}
        ${PYTHON_SOURCES}
        PARENT_SCOPE)
