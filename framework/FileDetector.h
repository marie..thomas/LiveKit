#ifndef LIVEKIT_FILEDETECTOR_H
#define LIVEKIT_FILEDETECTOR_H

#include <string>

class FileDetector {
private:

    std::string rootPath;

public:

    explicit FileDetector(std::string path);

    void checkForDirectory(std::string path);

    void checkForCycleFiles(int lanes, int cycle, bool isBgzfParser);

    void checkForAllLanes(int lanes, std::string path = "");
};

#endif //LIVEKIT_FILEDETECTOR_H