#ifndef LIVEKIT_AGGREGATEDBCLPARSER_H
#define LIVEKIT_AGGREGATEDBCLPARSER_H

#include <unordered_map>
#include "../data_representation/BclRepresentation.h"
#include "BclParser.h"
#include "FilterParser.h"
#include <sys/stat.h>
#include <boost/iostreams/filtering_stream.hpp> // File streams
#include <boost/iostreams/filter/gzip.hpp> // Read GZIP data vectors
#include <boost/filesystem.hpp>	// File handling
#include <zlib.h> // GZIP file streams
#include <cmath>

////////////////////////////////////////////////////////////
/////////// BCL BYTE CONVERTER AND BACK INSERTER ///////////
///// adapted from boost::iostream::back_insert_device /////
////////////////////////////////////////////////////////////

/**
 * Adaption of Boost's back_insert_device which converts the single bytes according to the number of quality and basecall bits in a CBCL file.
 */
template<typename Container>
class bcl_byte_converter_and_back_insert_device {

public:

    typedef typename Container::value_type char_type;
    typedef boost::iostreams::sink_tag category;

    /**
     * BCL byte transformer and back inserter.
     * Converts several basecalls stored in a single byte to one-byte-per-basecall format and stores it in a container.
     * @param cnt Reference to the container to store the transformed bytes.
     * @param qual_bits Number of bits per quality bin.
     * @param bc_bits Number of bits per base call.
     * TODO This does not convert the quality bins to actual quality values yet.
     */
    bcl_byte_converter_and_back_insert_device(Container &cnt, uint8_t qual_bits, uint8_t bc_bits,
                                              std::unordered_map<uint32_t, uint32_t> qual_map) : container(&cnt) {
        b_bits = bc_bits;
        q_bits = qual_bits;
        b_mask = std::pow(2, (bc_bits)) - 1;
        q_mask = std::pow(2, (qual_bits)) - 1;
        max_q = std::pow(2, 8 - bc_bits) - 1;
        set_qual_map(qual_map);
    }

    /**
     * Convert a single byte to one or more bytes according to the number of quality and basecall bits.
     * For bits per basecall >=5 (including nucleotide and quality), the data remains the same.
     * @param s The next byte.
     * @param n The size of the write stream.
     * @return Number of bytes added to the container.
     */
    std::streamsize write(const char_type *s, std::streamsize n) {
        uint8_t converted_bits = 0;
        // Count, how many bytes of the input stream have been processed
        std::streamsize pushed_bytes = 0;

        uint8_t total_bits = b_bits + q_bits;

        // Iterate over all bytes
        for (; n > 0; --n, ++s, ++pushed_bytes) {
            converted_bits = 0;

            // iterate over all basecall-quality pairs
            while (converted_bits + total_bits <= 8) {

                uint8_t bc = ((*s) >> converted_bits) & b_mask;
                uint8_t qual_bin = ((*s) >> (converted_bits + b_bits)) & q_mask;

                converted_bits += b_bits + q_bits;
                this->container->push_back(convert(bc, qual_bin));
            }
        }
        return pushed_bytes;
    }

protected:

    /** Number of bits per basecall. */
    uint8_t b_bits;

    /** Mask for basecall bits. */
    uint8_t b_mask;

    /** Number of bits per quality value. */
    uint8_t q_bits;

    /** Mask for quality bits. */
    uint8_t q_mask;

    /** Maximum quality value. */
    uint8_t max_q;

    /** Mapping for quality bins to actual quality values. */
    std::unordered_map<char_type, char_type> q_map;

    /** Container to push the transformed bytes. */
    Container *container;

private:

    /**
     * Convert a basecall and quality bin information to actual 1-byte coded format QQQQQQBB (Q=Quality, B=BaseCall)
     * @param bc The basecall
     * @param q_bin The quality bin
     * @return 1-byte coded base call format QQQQQQBB (Q=Quality, B=BaseCall)
     */
    char_type convert(char_type bc, char_type q_bin) {
        return ((q_map[q_bin] << b_bits) | bc);
    }

    /**
     * Convert the input uint32_t map to a char_type map.
     * @param qm Quality mal of type uint32_t to store.
     */
    void set_qual_map(std::unordered_map<uint32_t, uint32_t> qm) {
        for (auto bin = qm.begin(); bin != qm.end(); ++bin) {
            if (bin->first > q_mask) {
                throw std::runtime_error("Invalid quality bin >" + std::to_string(q_mask) + ".");
            }
            if (bin->second > max_q) {
                throw std::runtime_error("Invalid quality value >" + std::to_string(max_q) + ".");
            }
            q_map[char_type(bin->first)] = char_type(bin->second);
        }
    }
};

/**
 * Function to create a bcl_byte_converter_and_back_insert_device instance.
 * @param cnt Container to store the transformed bytes.
 * @param qual_bits Number of bits used for a quality bin.
 * @param bc_bits Number of bits used for one nucleotide.
 * @return A bcl_byte_converter_and_back_insert_device transforming multi-basecall bytes
 * to a data vector with 1 basecall per byte.
 */
template<typename Container>
bcl_byte_converter_and_back_insert_device<Container>
bcl_byte_converter_and_back_inserter(Container &cnt, uint8_t qual_bits, uint8_t bc_bits,
                                     std::unordered_map<uint32_t, uint32_t> qual_map) {
    return bcl_byte_converter_and_back_insert_device<Container>(cnt, qual_bits, bc_bits, qual_map);
}

/**
 * Tile information obtained from the header of a CBCL file.
 * Contains several information about the stored data for a tile, e.g.
 * tile number, number of clusters, compressed size, uncompressed size, pf excluded bit.
 */
struct CBCL_tile {

    /** The tile number. */
    uint32_t tile_number;    // Don't use TileType here, as the bit number in CBCL format is relevant!

    /** The number of clusters (reads). */
    uint32_t num_clusters;

    /** Size of the compressed CBCL block (in bytes). */
    uint32_t compressed_size;

    /** Size of the decompressed CBCL block (in bytes). */
    uint32_t uncompressed_size;

};

/**
 * Header information of a CBCL file.
 * Contains several information about the actual file format, e.g.
 * version, bits per quality/basecall, quality mapping, tile data ...
 */
struct CBCL_header {

    /** CBCL version. */
    uint16_t version;

    /** Size of the file header (in bytes). */
    uint32_t header_size;

    /** Number of quality bin / basecall bits. */
    uint8_t qual_bits, bc_bits;

    /** Size of the quality mapping. */
    uint32_t qual_map_size;

    /** The quality mapping. */
    std::unordered_map<uint32_t, uint32_t> qual_map;

    /** Number of tiles included in the file, i.e. stored in the tiles vector. */
    uint32_t tiles_size;

    /** Tiles stored in the file. */
    std::vector<CBCL_tile> tiles;

    /** non-PF clusters excluded flag, states whether filtered BaseCalls are excluded from the CBCL file */
    bool pf_excluded;
};


class AggregatedBclParser : public BclParser {
public:
    AggregatedBclParser(BclRepresentation *bclRepresentation, FilterRepresentation *filterRepresentation)
        : BclParser(bclRepresentation)
        , filterRepresentation(filterRepresentation) {}

    void parse(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &bclPath) override;

private:
    CBCL_header load_CBCL_header(FILE *&f_stream);

    CBCL_header getCBCLHeader(FILE *&fStream);

    struct stat cachedFileStat;

    CBCL_header cachedHeader;

    /** The AggregatedBclParser needs to access the FilterRepresentation because of the pf_excluded flag */
    FilterRepresentation *filterRepresentation;
};


#endif //LIVEKIT_AGGREGATEDBCLPARSER_H
