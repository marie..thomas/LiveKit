#ifndef LIVEKIT_PLUGINEXECUTIONGRAPH_H
#define LIVEKIT_PLUGINEXECUTIONGRAPH_H

#include <vector>
#include <string>
#include <map>
#include <deque>

#include "../Plugin.h"

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

class HelperPlugin : public Plugin {
public:
    explicit HelperPlugin(PluginSpecification *specification) {
        this->specification = specification;
    }

    void setConfig() override {};

    void init() override {};

    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override { return inputFragments; };

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override { return inputFragments; };

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override { return inputFragments; };

    void finalize() override {};
};

class PluginExecutionGraph
        : public boost::adjacency_list<boost::listS, boost::vecS, boost::directedS, Plugin *> {

    typedef boost::graph_traits<PluginExecutionGraph>::vertex_descriptor Vertex;
    typedef boost::graph_traits<PluginExecutionGraph>::edge_descriptor Edge;
    typedef boost::graph_traits<PluginExecutionGraph>::edge_iterator EdgeIterator;
    typedef boost::graph_traits<PluginExecutionGraph>::out_edge_iterator OutEdgeIterator;

    typedef struct {
        std::string type;
        Vertex vertex;
        std::string inputEdgeCategory;
    } OutputEdgeEntry;

#ifdef TEST_DEFINITIONS

    friend class PluginExecutionGraphAccessHelper;

#endif

    std::vector<std::vector<Edge>> invertedAdjacencyList;

    std::map<Edge, std::vector<PluginSpecification::FragmentSpecification>> edgeFragmentSpecifications;

    std::map<Edge, bool> isProcessed;

    std::deque<Vertex> executionQueue;

    std::map<std::string, OutputEdgeEntry> outputFragmentSpecificationToPlugin;

    std::vector<HelperPlugin *> helperPlugins = {};

    std::vector<Vertex> helperPluginVertices = {};

    Plugin *toPlugin(Vertex vertex);

    template<typename T>
    Edge toEdge(T edgePair) {
        Vertex sourceVertex = boost::source(*edgePair.first, *this);
        Vertex targetVertex = boost::target(*edgePair.first, *this);
        return this->getEdge(sourceVertex, targetVertex).first;
    }

    std::vector<PluginSpecification::FragmentSpecification> toFragmentSpecifications(Edge edge);

    bool allEdgesProcessed(Vertex pluginVertex);

    std::pair<Edge, bool> getEdge(Vertex sourceVertex, Vertex targetVertex);

    void setupInvertedAdjacencyList();

    void updateExecutionQueue(Vertex currentPluginVertex, FragmentContainer *outputFragmentContainer);

    void addOutputFragmentSpecificationToPluginMapping(
            PluginSpecification::FragmentSpecification outputFragmentSpecification, Vertex vertex,
            std::string inputEdgeCategory);

    Vertex addVertex(Plugin *plugin);

    Edge
    addEdge(Vertex sourceVertex, Vertex targetVertex, PluginSpecification::FragmentSpecification fragmentSpecification);

    void addInputEdgesForPlugin(Vertex targetVertex, Vertex startPluginVertex, Vertex endPluginVertex,
                                std::string inputEdgeCategory);

    void setupSubGraph(Vertex startPluginVertex, Vertex endPluginVertex, std::string edgeCategory,
                       std::set<Plugin *> plugins);

    void executePlugins(FragmentContainer *(Plugin::*hook)(FragmentContainer *), Vertex startPluginVertex,
                        Vertex endPluginVertex);

public:
    /*
     * Holds one fragment container for every plugin.
     */
    std::vector<FragmentContainer *> preparedInputContainer;

    /**
     * Executes runPreprocessing() for every plugin considering the dependencies between the plugins.
     */
    void runPreprocessing();

    /**
    * Executes runCycle() for every plugin considering the dependencies between the plugins.
    */
    void runCycle();

    /**
    * Executes runFullReadPostprocessing() for every plugin considering the dependencies between the plugins.
    */
    void runFullReadPostprocessing();

    /**
     * Inserts the plugins from the set into three coherent sub graphs for preprocessing, normal cycles and postprocessing.
     * Creates edges for every fragment that occurs in one plugin's´inputFragment´ and another plugin's ´outputFragment´ specification.
     * @param plugins The plugins to be added to the graph
     */
    void addPlugins(std::set<Plugin *> plugins);

    ~PluginExecutionGraph();
};

#endif //LIVEKIT_PLUGINEXECUTIONGRAPH_H
