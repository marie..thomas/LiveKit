#include <vector>
#include "FragmentContainer.h"

using namespace std;

shared_ptr<Fragment> FragmentContainer::get(string name) {
    return this->fragments.at(name);
};

bool FragmentContainer::has(string name) {
    return this->fragments.count(name) > 0;
};

void FragmentContainer::clear() {
    this->fragments.clear();
}

shared_ptr<Fragment> FragmentContainer::add(shared_ptr<Fragment> fragment) {

    if (this->fragments.count(fragment->name)) {
        throw "Fragment with name " + fragment->name + " already exists in this FragmentContainer!";
    }
    this->fragments[fragment->name] = fragment;
    return fragment;
};

vector<shared_ptr<Fragment>> FragmentContainer::getAllFragments() {
    vector<shared_ptr<Fragment>> fragments;

    for (auto const &fragment : this->fragments) {
        fragments.push_back(fragment.second);
    }

    return fragments;
}
