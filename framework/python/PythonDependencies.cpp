#include "PythonDependencies.h"
#include <iostream>
#include <vector>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include "../Framework.h"
#include "PythonFrameworkInterface.h"

typedef std::vector<std::string> StringList;
typedef std::vector<char> NucleotideList;
typedef std::vector<NucleotideList> SequenceList;

namespace bp = boost::python;

BOOST_PYTHON_MODULE (PluginLibrary) {
    bp::class_<StringList>("StringList")
            .def(bp::vector_indexing_suite<StringList>());

    bp::class_<NucleotideList>("NucleotideList")
            .def(bp::vector_indexing_suite<NucleotideList>());

    bp::class_<SequenceList>("SequenceList")
            .def(bp::vector_indexing_suite<SequenceList>());

    bp::class_<SequenceContainer>("SequenceContainer")
            .def("getSequence", &SequenceContainer::getSequence, bp::return_value_policy<bp::reference_existing_object>())
            .def("getAllSequences", &SequenceContainer::getAllSequences, bp::return_value_policy<bp::reference_existing_object>());

    //bp::class_<FrameworkInterface>("FrameworkInterface", bp::init<Framework*>());

    bp::class_<PythonFrameworkInterface>("PythonFrameworkInterface", bp::init<FrameworkInterface *>())
            .def("getSequences", &PythonFrameworkInterface::getSequences,
                 bp::return_value_policy<bp::reference_existing_object>())
            .def("getSequence", &PythonFrameworkInterface::getSequence, bp::return_value_policy<bp::reference_existing_object>())
            .def("getBcl", &PythonFrameworkInterface::getBcl, bp::return_value_policy<bp::reference_existing_object>())
            .def("getBcls", &PythonFrameworkInterface::getBclsVector)
            .def("getMostRecentBcls", &PythonFrameworkInterface::getMostRecentBclsVector)
            .def("filterBasecall", &PythonFrameworkInterface::filterBasecall)
            .def("getNumSequences", &PythonFrameworkInterface::getNumSequences)
            .def("getCurrentCycle", &PythonFrameworkInterface::getCurrentCycle)
            .def("getCycleCount", &PythonFrameworkInterface::getCycleCount)
            .def("getCurrentReadCycle", &PythonFrameworkInterface::getCurrentReadCycle)
            .def("getCurrentReadId", &PythonFrameworkInterface::getCurrentReadId)
            .def("getCurrentReadLength", &PythonFrameworkInterface::getCurrentReadLength)
            .def("isBarcodeCycle", &PythonFrameworkInterface::isBarcodeCycle)
            .def("getCurrentMateId", &PythonFrameworkInterface::getCurrentMateId)
            .def("getMateCount", &PythonFrameworkInterface::getMateCount);
}

void PythonDependencies::initPython() {
    try {
        PyImport_AppendInittab("PluginLibrary", &PyInit_PluginLibrary);
        Py_Initialize();

        boost::filesystem::path workingDir = boost::filesystem::absolute("./../plugins/python").normalize();
        PyObject * sysPath = PySys_GetObject((char *) "path");
        PyList_Insert(sysPath, 0, PyUnicode_FromString(workingDir.string().c_str()));

    } catch (const bp::error_already_set &) {
        std::cerr << "Python Error: ";
        PyErr_Print();
        throw std::runtime_error("<<< Python exception caught!");
    }
}
