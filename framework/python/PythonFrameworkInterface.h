#ifndef LIVEKIT_PYTHONFRAMEWORKINTERFACE_H
#define LIVEKIT_PYTHONFRAMEWORKINTERFACE_H

#include "../Framework.h"

class PythonFrameworkInterface : public FrameworkInterface {
public:
    explicit PythonFrameworkInterface(FrameworkInterface *frameworkInterface) : FrameworkInterface(
            *frameworkInterface) {};

    const std::vector<BCL> getBclsVector(uint16_t lane, uint16_t tile) {
        auto deque = FrameworkInterface::getBcls(lane, tile);
        return std::vector<BCL>(deque.begin(), deque.end());
    };

    std::vector<BCL> getMostRecentBclsVector(uint16_t lane) {
        auto deque = FrameworkInterface::getMostRecentBcls(lane);
        return std::vector<BCL>(deque.begin(), deque.end());
    };
};


#endif //LIVEKIT_PYTHONFRAMEWORKINTERFACE_H
