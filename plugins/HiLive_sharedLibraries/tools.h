/**
 * This class provides functions that are dependent of the alignmentSettings!
 * For functions that are not dependent of any other HiLive class, please use the tools_static class!
 * Please do NOT add further includes to this file since this will lead to unwanted dependencies!
 */

#ifndef TOOLS_H
#define TOOLS_H

/* DONT ADD ANY INCLUDES */
#include "tools_static.h"
#include "global_variables.h"
/* DONT ADD ANY INCLUDES */


//////////////////////////////////////
////////// Build file names //////////
//////////////////////////////////////

/**
 * Get the name of a bcl file.
 * @param ln The lane number.
 * @param tl The tile number.
 * @param cl The sequencing cycle.
 * @return Path to the bcl file.
 */
inline std::string get_bcl_fname(uint16_t ln, uint16_t tl, uint16_t cl, std::string rootDir) {
  std::ostringstream path_stream;
  path_stream << rootDir << "/L" << to_N_digits(ln,3) << "/C" << cl << ".1/s_"<< ln <<"_" << tl << ".bcl";
  return path_stream.str();
}

/**
 * Get the name of an alignment file.
 * @param ln The lane number.
 * @param tl The tile number.
 * @param cl The cycle for the respective mate.
 * @param mt The mate number.
 * @return Path to the alignment file.
 */
inline std::string get_align_fname(uint16_t ln, uint16_t tl, uint16_t cl, uint16_t mt, std::string &tempDir){
  std::ostringstream path_stream;
  path_stream << tempDir << "/L" << to_N_digits(ln,3) << "/s_"<< ln << "_" << tl << "." << mt << "."<< cl << ".align";
  return path_stream.str();
}

////////////////////////////////////
////////// SAM/BAM output //////////
////////////////////////////////////

/**
 * Get the header for a SAM/BAM output file.
 * @return The BAM header.
 * @author Tobias Loka
 */
seqan::BamHeader getBamHeader();

/** Reverse a MD:Z tag for reverse alignments. */
std::string reverse_mdz(std::string mdz);


/////////////////////////////////
////////// Other stuff //////////
/////////////////////////////////

/**
 * Copy a file while locking them in the global fileLocks.
 */
int atomic_rename( const char *oldname, const char *newname );

#endif /* TOOLS_H */
