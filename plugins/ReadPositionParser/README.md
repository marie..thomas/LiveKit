# ReadPositionParser Plugin

This plugin parses the position files, that are used by the Bcl2FastQ plugin. <br>
These position files indicate where the sequences are located at the flowcell. <br>
There are three different types of positions files: *pos.txt, .locs and .clocs. <br>
All of them are supported by this plugin. The parsed positions are outputed as a fragment and can be received by any other plugin if necessary. <br>
