PathoLive - Description
=======================================

Description
-----------

PathoLive is a real time pathogen diagnostics tool that detects pathogenic 
sequences from metagenomic Illumina sequencing data while they are
produced. This means, a hint towards a diagnosis is provided before the
sequencer is finished. 


Website
-------

This plugin is based on the PathoLive project.
- website is https://gitlab.com/SimonHTausch/PathoLive

There you can find the latest version of PathoLive, source code, documentation,
and examples.

Dependencies
------------
- python3 -m pip install pysam
- python3 -m pip install matplotlib

Usage
-----

The PathoLive plugin needs to run in combination with the Hilive plugin.


It is important that the configs of these two plugins match (see example below).
PathoLive works with the SAM/BAM files from Hilive. 
Therefore it is important that in each cycle Hilive is run before PathoLive. 
This is achieved by using a dummy fragment, which defines this order but does act as a real fragment (because it is never used).

Example PathoLive config:

```
{
  "name" : "PathoLive",
  "outDir" : "../../PathoLive_out",
  "outFormat" : "BAM",
  "outCycles" : [20,50,100]
}
```

Matching config for HiLive:

```
{
  ...
  "outDir": "../../PathoLive_out",
  "outputFormat": "B",
  "outputCycles": "20,50,100"
}
```

The plugin structure in the framework config looks like this:
```
{
  "plugins": [
    ...
    {
      "pluginPath": "./../plugins/binary/libHilive_Alignment.so",
      "configPath": "./../config/local/hilive.json",
      "inputFragments": {
        "preprocessing": [...],
        "cycle": [...]
      },
      "outputFragments": {
        "cycle": [
          {
            "name": "dummy",
            "type": "DUMMY"
          }
        ]
      }
    },
    {
      "pluginPath": "../plugins/python/PathoLive/PathoLive.py",
      "configPath": "../config/pathoLive.json",
      "inputFragments": {
        "cycle" : [
          {
            "name": "dummy",
            "type": "DUMMY"
          }
        ]
      },
      "outputFragments": {}
    }
  ],
  ...
}
```

Building your own reference database
------------------------------------

We advise you to use the reference database provided by the PathoLive team.  
You find the prebuilt database here: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2536788.svg)](https://doi.org/10.5281/zenodo.2536788)  
  
Use the ``hilive-build`` executable of HiLive2 to build the index that is required for live analysis.  
  
However, you can also build your own reference database.  
Disclaimer: This requires to download several hundreds of gigabytes of data. 

Make sure to install the following additional dependencies:
 * bowtie2
 * fastq-dump
 * trimmomatic

Build a Bowtie2 index as well as a HiLive index of your reference file. 
Insert the paths to the Bowtie2 index and a result folder and start the bash-script 
    ``../background_definition/download_trim_map_background.bsh``.

Then start the python script ``calculate_overall_background_coverage.py`` with the provided result folder as input folder and replace the file ``prelim_data/background_coverages.pickle`` in your PathoLive-folder by the new one.


License
-------

See the file LICENSE.md for licensing information.
