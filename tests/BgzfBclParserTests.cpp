#include <iostream>
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/parser/BclParser.h"
#include "../framework/basecalls/parser/BgzfBclParser.h"
#include "./catch2/catch.hpp"

using namespace std;

TEST_CASE("BgzfBclParserTests") {
    vector<uint16_t> lanes = {1};
    vector<uint16_t> tiles = {11101, 11102, 11103};
    BclRepresentation bclRepresentation(lanes, tiles); // 1 lane, 3 tiles
    BgzfBclParser bclParser(&bclRepresentation);
    string filename = "../tests/mockFiles/BgzfBaseCalls";

    SECTION("Should be able to parse single bgzf file") {
        bclParser.parse(1, 11101, 1, filename);
        auto bcl = bclParser.bclRepresentation->getBcl(1, 11101, 0); // first lane and first tile

        // Check if the correct number of reads were parsed which is encoded in the .bci file of every lane
        REQUIRE(bclParser.bclRepresentation->getBclSize(1, 11101) == 655895);


        // TODO: Check if content of bcl is correctly parsed
    }

    SECTION("Should print an error if bgzf file doesn't exist") {
        REQUIRE_THROWS_WITH(bclParser.parse(2, 11101, 1, filename),
                            "Cannot read file: ../tests/mockFiles/BgzfBaseCalls/L002/0001.bcl.bgzf");
    }
}

